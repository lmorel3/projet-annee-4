package fr.cpe.abalown.utils;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileUtils {

    public static List<String> listFiles(String resourceDirectory) {
        URL appearances = FileUtils.class.getResource(resourceDirectory);

        try {
            if (appearances != null) {
                final File dir = new File(appearances.toURI());
                return new ArrayList<>(Arrays.asList(dir.list()));
            }
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
        }

        return new ArrayList<>();
    }

}
