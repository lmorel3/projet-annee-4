
package fr.cpe.abalown.utils;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.models.BoardCoord;
import fr.cpe.abalown.models.VisualCoord;

public class CoordConverter {

    private float[] offsetsX = { 2.0f, 1.5f, 1.0f, 0.5f, 0.0f, 0.5f, 1.0f, 1.5f, 2.0f };

    private int boardOffsetX = ContextHolder.getInstance().isSmallSized() ? 43 : 90;
    private int boardOffsetY = ContextHolder.getInstance().isSmallSized() ? 67 : 136;

    private double cellHeight = ContextHolder.getInstance().getBallHeight();
    private double cellWidth = ContextHolder.getInstance().getBallWidth();

    public VisualCoord getVisualCoordFromBoardCoord(BoardCoord coord) {
        return getVisualCoordFromBoardCoord(coord.getCol(), coord.getRow());
    }

    public VisualCoord getVisualCoordFromBoardCoord(int xBoard, int yBoard) {

        // modificateur de la coordonnée x
        //  si la Coord est dans la moitié haute du plateau
        int xDiff = 0;

        if (yBoard >= 5) {
            // moitié haute du plateau
            xDiff = - (yBoard - 4);
        }

        int xVisual =       boardOffsetX
                          + (int) ((offsetsX[yBoard]) * cellWidth)
                          + (int) ((double)(xDiff + xBoard) * cellWidth);

        int yVisual = boardOffsetY
                    + (int) (8.0f * cellHeight)
                    - (int) ((double)yBoard * cellHeight);

        return new VisualCoord(xVisual, yVisual);
    }

    public BoardCoord getBoardCoordFromVisualCoord(VisualCoord coord) {
        return getBoardCoordFromVisualCoord(coord.getX(), coord.getY());
    }

    public BoardCoord getBoardCoordFromVisualCoord(int xVisual, int yVisual) {

        int yBoard = 8 - (int) Math.floor((yVisual - boardOffsetY)
                                    / cellHeight);


        // modificateur de la coordonnée x
        //  si la Coord est dans la moitié haute du plateau
        int xDiff = 0;

        if (yBoard >= 5) {
            // moitié haute du plateau
            xDiff = - (yBoard - 4);
        }

        int xBoard = (int) ((xVisual
                            - boardOffsetX
                            - offsetsX[yBoard] * cellWidth)
                            / cellWidth)
                            - xDiff;

        return new BoardCoord(xBoard, yBoard);
    }

}