package fr.cpe.abalown.utils;

import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.models.BoardBall;
import fr.cpe.abalown.models.BoardCoord;
import fr.cpe.abalown.models.Game;

import java.util.Map;

public class DrawGameToConsole {

    public static String drawArrayToConsole(Game game) {
        String result = "";

        int startOffset = 4;
        int endOffset = 8;

        Map<BoardCoord, BoardBall> blackBalls = game.getPlayerBoardBalls(Color.BLACK).getBalls();
        Map<BoardCoord, BoardBall> whiteBalls = game.getPlayerBoardBalls(Color.WHITE).getBalls();

        result += "  ";
        for (int j = 0; j < 9; j++) {
            result += String.format("%d ", j);
        }
        result += "\n";

        for (int i = 8; i >= 0; i--) {
            result += String.format("%d ", i);
            for (int j = 0; j < 9; j++) {
                if (j < startOffset || j > endOffset) {
                    result += "  ";
                } else if (blackBalls.containsKey(new BoardCoord(i, j))) {
                    result += "B ";
                } else if (whiteBalls.containsKey(new BoardCoord(i, j))) {
                    result += "W ";
                } else {
                    result += ". ";
                }
            }
            result += "\n";
            if (i > 4) {
                startOffset--;
            } else {
                endOffset--;
            }
        }

        return result;
    }

    public static String drawHexagonToConsole(Game game) {
        String result = "";

        int startOffset = 4;
        int endOffset = 0;
        int charOffsetCount = 4;

        Map<BoardCoord, BoardBall> blackBalls = game.getPlayerBoardBalls(Color.BLACK).getBalls();
        Map<BoardCoord, BoardBall> whiteBalls = game.getPlayerBoardBalls(Color.WHITE).getBalls();

        for (int i = 8; i >= 0; i--) {
            String charOffset = new String(new char[charOffsetCount]).replace("\0", " ");
            result += charOffset;
            for (int j = startOffset; j < 9 - endOffset; j++) {
                if (blackBalls.containsKey(new BoardCoord(i, j))) {
                    result += "B ";
                } else if (whiteBalls.containsKey(new BoardCoord(i, j))) {
                    result += "W ";
                } else {
                    result += ". ";
                }
            }
            result += "\n";
            if (i > 4) {
                charOffsetCount--;
                startOffset--;
            } else {
                charOffsetCount++;
                endOffset++;
            }
        }

        return result;
    }
}
