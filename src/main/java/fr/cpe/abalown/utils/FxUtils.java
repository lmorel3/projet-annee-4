package fr.cpe.abalown.utils;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class FxUtils {

    /**
     * Loads the given view
     *
     * @param name : name of FXML file
     */
    public static void loadView(String name, Node node) {
        try {

            Parent root = getView(name);
            System.out.println("View is " + root.toString());

            Stage stage = (Stage) node.getScene().getWindow();
            stage.setScene(new Scene(root, getWindowWidth(), getWindowHeight()));
            stage.setFullScreen(ContextHolder.getInstance().isFullScreen());

        } catch (Exception e) {
            System.err.println("Unable to load game " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Loads a FXML
     *
     * @param name
     * @return
     * @throws IOException
     */
    public static Parent getView(String name) throws Exception {
        String dir = (ContextHolder.getInstance().isSmallSized()) ? "small" : "large";

        URL file = FxUtils.class.getClassLoader().getResource("views/" + dir + "/" + name + ".fxml");
        System.out.println(" getView " + file);

        if (file == null) {
            throw new Exception("Unable to load view " + name);
        }

        return FXMLLoader.load(file);
    }

    public static void centerStage(Stage stage) {
        stage.sizeToScene();

        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);
    }


    public static double getWindowWidth() {
        return ContextHolder.getInstance().isSmallSized() ? GameConfig.APP_WIDTH_SMALL : GameConfig.APP_WIDTH_LARGE;
    }

    public static double getWindowHeight() {
        return ContextHolder.getInstance().isSmallSized() ? GameConfig.APP_HEIGHT_SMALL : GameConfig.APP_HEIGHT_LARGE;
    }

}
