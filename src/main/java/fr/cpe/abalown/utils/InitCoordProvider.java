package fr.cpe.abalown.utils;

import fr.cpe.abalown.models.BoardBall;
import fr.cpe.abalown.models.BoardCoord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InitCoordProvider {

    /**
     * @return White init coords
     */
    public static Map<BoardCoord, BoardBall> getWhiteInitCoord() {
        return new HashMap<BoardCoord, BoardBall>() {
            {
                // First line
                put(new BoardCoord(0,0), new BoardBall());
                put(new BoardCoord(0,1), new BoardBall());
                put(new BoardCoord(0,2), new BoardBall());
                put(new BoardCoord(0,3), new BoardBall());
                put(new BoardCoord(0,4), new BoardBall());

                // Second line
                put(new BoardCoord(1,0), new BoardBall());
                put(new BoardCoord(1,1), new BoardBall());
                put(new BoardCoord(1,2), new BoardBall());
                put(new BoardCoord(1,3), new BoardBall());
                put(new BoardCoord(1,4), new BoardBall());
                put(new BoardCoord(1,5), new BoardBall());

                // Third line
                put(new BoardCoord(2,2), new BoardBall());
                put(new BoardCoord(2,3), new BoardBall());
                put(new BoardCoord(2,4), new BoardBall());
            }
        };
    }

    /**
     * @return Black init coords
     */
    public static Map<BoardCoord, BoardBall> getBlackInitCoord() {
        return new HashMap<BoardCoord, BoardBall>() {
            {
                // First line
                put(new BoardCoord(8,4), new BoardBall());
                put(new BoardCoord(8,5), new BoardBall());
                put(new BoardCoord(8,6), new BoardBall());
                put(new BoardCoord(8,7), new BoardBall());
                put(new BoardCoord(8,8), new BoardBall());

                // Second line
                put(new BoardCoord(7,3), new BoardBall());
                put(new BoardCoord(7,4), new BoardBall());
                put(new BoardCoord(7,5), new BoardBall());
                put(new BoardCoord(7,6), new BoardBall());
                put(new BoardCoord(7,7), new BoardBall());
                put(new BoardCoord(7,8), new BoardBall());

                // Third line
                put(new BoardCoord(6,4), new BoardBall());
                put(new BoardCoord(6,5), new BoardBall());
                put(new BoardCoord(6,6), new BoardBall());
            }
        };
    }

    public static List<BoardCoord> getCellsCoords() {
        List<BoardCoord> coords = new ArrayList<>();
        for (int i = 0; i <= 8; i++) {
            for (int j = 0; j <= 8; j++) {
                coords.add(new BoardCoord(i, j));
            }
        }
        return coords;
    }

}
