package fr.cpe.abalown.utils;

import fr.cpe.abalown.enums.Direction;
import fr.cpe.abalown.models.BoardCoord;

import java.util.ArrayList;
import java.util.List;

public class BoardCoordFinder {

    public static List<BoardCoord> findCoords(List<BoardCoord> coords, Direction direction) {
        List<BoardCoord> newCoords = new ArrayList<>();

        // HexaMove -> 2dim ArrayMove
        // DownRight -> Down
        // DownLeft -> DiagDownLeft
        // UpRight -> DiagUpRight
        // UpLeft -> Up
        // Left -> Left
        // Right -> Right

        int rowOffset = 0;
        int colOffset = 0;

        switch (direction) {

            case UPLEFT:
                rowOffset = 1;
                break;
            case UPRIGHT:
                rowOffset = 1;
                colOffset = 1;
                break;
            case LEFT:
                colOffset = -1;
                break;
            case RIGHT:
                colOffset = 1;
                break;
            case DOWNLEFT:
                rowOffset = -1;
                colOffset = -1;
                break;
            case DOWNRIGHT:
                rowOffset = -1;
                break;
            default:
                break;
        }

        for (BoardCoord coord : coords) {
            newCoords.add(new BoardCoord(coord.getRow() + rowOffset, coord.getCol() + colOffset));
        }

        return newCoords;
    }
}
