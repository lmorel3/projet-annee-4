package fr.cpe.abalown.enums;

public enum Direction {

    UPLEFT,
    UPRIGHT,
    LEFT,
    RIGHT,
    DOWNLEFT,
    DOWNRIGHT

}