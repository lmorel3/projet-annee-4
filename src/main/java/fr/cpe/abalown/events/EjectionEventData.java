package fr.cpe.abalown.events;

import fr.cpe.abalown.enums.Color;

public class EjectionEventData {

    private Color color;
    private int remainingPlayerBalls;

    public EjectionEventData(Color color, int remainingPlayerBalls) {
        this.color = color;
        this.remainingPlayerBalls = remainingPlayerBalls;
    }

    public Color getColor() {
        return color;
    }

    public int getRemainingPlayerBalls() {
        return remainingPlayerBalls;
    }
}
