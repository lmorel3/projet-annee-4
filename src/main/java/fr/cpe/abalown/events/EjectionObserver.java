package fr.cpe.abalown.events;

import fr.cpe.abalown.controllers.GameController;
import fr.cpe.abalown.enums.Color;

import java.util.Observable;
import java.util.Observer;

public class EjectionObserver implements Observer {

    private GameController gameController;

    public EjectionObserver(GameController gameController) {
        this.gameController = gameController;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("EjectionObserver update()  " + o.getClass() + arg.getClass());
        gameController.onBallEjected((EjectionEventData) arg);
    }
}
