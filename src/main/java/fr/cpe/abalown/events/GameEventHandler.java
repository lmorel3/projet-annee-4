package fr.cpe.abalown.events;

import fr.cpe.abalown.controllers.GameController;
import fr.cpe.abalown.views.BallGui;
import fr.cpe.abalown.views.CellGui;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 */
public class GameEventHandler implements EventHandler<MouseEvent> {

    private GameController gameController;

    private GameEventHandler() {}

    public GameEventHandler(GameController gameController) {
        this.gameController = gameController;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {

            Object source = mouseEvent.getSource();

            //BallGui source = (BallGui)
            System.out.println("Left click on " + source.getClass() + " : " + source);


            //source.toggleSelected();

            //System.out.println("Board coords are " + source.getBoardCoord());
            // TODO: Call game method here

            if (source instanceof BallGui) {
                //((BallGui)source).toggleSelected();
                gameController.onClick((BallGui) source);
            } else if (source instanceof CellGui) {
                gameController.onClick((CellGui) source);
            }

        } else if (mouseEvent.getButton().equals(MouseButton.SECONDARY)) {
            // right click -> cancel selection
            gameController.cancelSelection();
        }

    }
}
