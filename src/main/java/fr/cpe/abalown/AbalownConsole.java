package fr.cpe.abalown;

import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.models.Game;
import fr.cpe.abalown.models.Player;

/**
 * Created by lmorel on 16/10/17.
 */
public class AbalownConsole {

    public static void main(String[] args) {
        Game game = new Game();
        System.out.println(game.toString());
    }
}
