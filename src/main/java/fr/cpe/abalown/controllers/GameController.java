package fr.cpe.abalown.controllers;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;
import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.enums.Direction;
import fr.cpe.abalown.events.EjectionEventData;
import fr.cpe.abalown.events.EjectionObserver;
import fr.cpe.abalown.events.GameEventHandler;
import fr.cpe.abalown.factories.BallGuiFactory;
import fr.cpe.abalown.factories.CellGuiFactory;
import fr.cpe.abalown.models.Game;
import fr.cpe.abalown.models.BoardCoord;
import fr.cpe.abalown.models.BoardBall;
import fr.cpe.abalown.models.BoardBalls;
import fr.cpe.abalown.utils.FxUtils;
import fr.cpe.abalown.utils.InitCoordProvider;
import fr.cpe.abalown.views.BallGui;
import fr.cpe.abalown.views.BoardGui;
import fr.cpe.abalown.views.CellGui;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.scene.image.ImageView;
import javafx.event.ActionEvent;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Optional;

public class GameController implements Initializable {

    @FXML
    private Pane mainPane;

    @FXML
    private BoardGui boardGui;

    @FXML
    private Text blackPlayerName;

    @FXML
    private Text whitePlayerName;

    @FXML
    private ImageView whiteTurn;

    @FXML
    private ImageView blackTurn;

    @FXML
    private ProgressBar blackBar;

    @FXML
    private ProgressBar whiteBar;

    @FXML
    private Button quitButton;

    private Game game;

    private GameEventHandler gameEventHandler;

    private EjectionObserver ejectionObserver;

    private Color currentPlayer = Color.WHITE;
    private List<BoardCoord> selectedBalls;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initGame();
        System.out.println("GameController is ready");
        quitButton.setOnAction(this::handleQuitButton);
    }

    private void initGame() {
        gameEventHandler = new GameEventHandler(this);

        game = new Game();

        ejectionObserver = new EjectionObserver(this);
        game.addObserver(ejectionObserver);

        displayContext();
        displayCells();
        displayBoards();

        // Noir commence
        selectedBalls = new ArrayList<>();
        switchPlayer();

        displayCells();
        updateBoardGui();
    }

    private void switchPlayer() {
        if (currentPlayer.equals(Color.BLACK)) {
            currentPlayer = Color.WHITE;
            whiteTurn.setVisible(true);
            blackTurn.setVisible(false);
        } else {
            currentPlayer = Color.BLACK;
            whiteTurn.setVisible(false);
            blackTurn.setVisible(true);
        }

        selectedBalls.clear();
    }

    private void displayBoards() {
        displayBoard(Color.BLACK, game.getPlayerBoardBalls(Color.BLACK));
        displayBoard(Color.WHITE, game.getPlayerBoardBalls(Color.WHITE));
    }

    private void displayBoard(Color color, BoardBalls boardBalls) {
        BallGuiFactory ballGuiFactory = new BallGuiFactory();

        for (Map.Entry<BoardCoord, BoardBall> coord : boardBalls.getBalls().entrySet()) {
            BallGui ball = ballGuiFactory.createBall(color, coord.getKey());
            ball.addEventHandler(MouseEvent.MOUSE_CLICKED, gameEventHandler);
            boardGui.addBall(ball);
        }
    }

    private void displayContext() {
        blackPlayerName.setText(ContextHolder.getInstance().getPlayerName(Color.BLACK));
        whitePlayerName.setText(ContextHolder.getInstance().getPlayerName(Color.WHITE));

        blackBar.setProgress(1.0);
        whiteBar.setProgress(1.0);
    }

    private void displayCells() {
        CellGuiFactory cellGuiFactory = new CellGuiFactory();
        for (BoardCoord coord : InitCoordProvider.getCellsCoords()) {
            CellGui cell = cellGuiFactory.createCell(coord);
            cell.addEventHandler(MouseEvent.MOUSE_CLICKED, gameEventHandler);
            cell.highlightOff();
            boardGui.addCell(cell);
        }
    }

    private void displayPossibleMoves() {
        List<BoardCoord> candidates = new ArrayList<>();
        // on regarde les cases adjaçentes à chaque boule sélectionnée
        for (BoardCoord selectedBall : selectedBalls) {
            for (Direction direction : Direction.values()) {
                candidates.add(game.getCoordOfThisDirection(selectedBall, direction));
            }
        }
        candidates.removeAll(selectedBalls);

        for (BoardCoord candidate : candidates) {
            Direction direction = game.determineDirection(selectedBalls, candidate);
            if (game.canMove(selectedBalls, direction)) {
                boardGui.highlightCell(candidate);
            }
        }
    }

    private void hidePossibleMoves() {
        for (BoardCoord selectedBall : selectedBalls) {
            for (Direction direction : Direction.values()) {
                BoardCoord ball = game.getCoordOfThisDirection(selectedBall, direction);
                boardGui.unHighlightCell(ball);
            }
        }
    }

    public void onClick(BallGui ballGui) {

        Color ballColor = ballGui.getColor();

        if (ballColor == currentPlayer) {
            // la boule cliquée est au joueur courant

            if (selectedBalls.contains(ballGui.getBoardCoord())) {
                //System.out.println("Already selected");
                // c'est une boule déjà sélectionnée
                // on désélectionne
                hidePossibleMoves();
                selectedBalls.remove(ballGui.getBoardCoord());
                ballGui.unSelect();
                displayPossibleMoves();
            } else if (selectedBalls.size() < 3) {
                // pas plus de 3 boules sélectionnées
                // on sélectionne
                hidePossibleMoves();
                selectedBalls.add(ballGui.getBoardCoord());
                ballGui.select();
                displayPossibleMoves();
            }
        } else {
            // la boule cliquée est au joueur adverse
            //  -> tentative de sumito

            if (!selectedBalls.isEmpty()) {
                // si on a sélectionné au moins une boule

                // on regarde la direction dans laquelle on veut déplacer les boules
                Direction direction = game.determineDirection(
                        selectedBalls,
                        ballGui.getBoardCoord()
                );

                if (game.canMove(selectedBalls, direction)) {
                    // le mouvement est autorisé

                    hidePossibleMoves();

                    // on rassemble les boules qu'on va déplacer
                    List<BoardCoord> ballsToMove = new ArrayList<>(selectedBalls);

                    // on y ajoute la(les) boule(s) poussée(s) aux boules qu'il faut déplacer
                    ballsToMove.addAll(game.getBallsPushedByThisMove(selectedBalls, direction));
                    System.out.print("BallsToMove : ");
                    for (BoardCoord b : ballsToMove) {
                        System.out.print(b.toString());
                    }
                    game.doMove(ballsToMove, direction);
                    updateBoardGui();
                    switchPlayer();
                } else {

                    // mouvement non autorisé
                    cancelSelection();
                    System.out.println("cannot move");
                }
            }
        }

    }

    public void onClick(CellGui cellGui) {
        // clic sur une case vide

        if (selectedBalls.size() == 0) {
            return;
        }

        System.out.println("Clicking on cell " + cellGui);
        Direction direction = game.determineDirection(selectedBalls, cellGui.getBoardCoord());
        if (game.canMove(selectedBalls, direction)) {
            // mouvement autorisé -> on bouge
            System.out.println("canMove");
            hidePossibleMoves();
            game.doMove(selectedBalls, direction);
            updateBoardGui();
            switchPlayer();
        } else {
            // mouvement non autorisé
            cancelSelection();
            System.out.println("cannot move");
        }

    }

    /**
     * Unselect the selected balls
     */
    public void cancelSelection() {
        hidePossibleMoves();
        selectedBalls.clear();
        updateBoardGui();
    }

    /**
     * Update board and balls display
     */
    private void updateBoardGui() {
        System.out.println("Resetting GameGui");
        boardGui.resetBalls();

        selectedBalls.clear(); // Déselectionne les boules

        displayBoards();
    }

    /**
     * Called when a ball is (just about to be) ejected from the board
     *
     * @param data data related to this event
     */
    public void onBallEjected(EjectionEventData data) {
        System.out.println("onBallEjected()");
        decrementProgressBar(data.getColor(), data.getRemainingPlayerBalls());

        if (data.getRemainingPlayerBalls() <= 8) {
            showVictoryAlert(data.getColor());
        }

        //BoardCoord coord = game.getEjectedBall();
    }

    private void showVictoryAlert(Color looser) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Victory!");
        alert.setHeaderText("Player " + looser + " loses the game");

        ButtonType buttonMainMenu = new ButtonType("Main Menu");
        ButtonType buttonCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonMainMenu, buttonCancel);

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == buttonMainMenu) {
            FxUtils.loadView("main", boardGui);
        }

    }

    private void decrementProgressBar(Color color, int remainingBalls) {
        ProgressBar bar = color.equals(Color.BLACK) ? blackBar : whiteBar;

        double value = ((double) remainingBalls - 8d) / 6d;
        bar.setProgress(value);

        System.out.println(color + " bar is now :" + remainingBalls + "-8m" + "/6 =" + value);
    }

    private void handleQuitButton(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Quit Game");
        alert.setHeaderText("Return to main menu or desktop");

        ButtonType buttonMainMenu = new ButtonType("Main Menu");
        ButtonType buttonDesktop = new ButtonType("To Desktop");
        ButtonType buttonCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonMainMenu, buttonDesktop, buttonCancel);

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == buttonMainMenu) {
            FxUtils.loadView("main", boardGui);
        } else if (result.get() == buttonDesktop) {
            Platform.exit();
        }
    }

}
