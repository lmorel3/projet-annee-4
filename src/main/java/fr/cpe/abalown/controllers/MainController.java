package fr.cpe.abalown.controllers;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;
import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.utils.FxUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    
    @FXML
    private Button playBtn;
    
    @FXML
    private Button settingsBtn;

    @FXML
    private Button customizeBtn;

    @FXML
    private Button leaderBoardBtn;

    @FXML
    private Button replayBtn;
    
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        init();
        handleButtons();
    }
    
    /**
     * Asks for users pseudos
     */
    private void init() {
        if (ContextHolder.getInstance().getPlayerName(Color.BLACK).equals("undefined")) {
            waitForPseudo(Color.BLACK);
        }
        if (ContextHolder.getInstance().getPlayerName(Color.WHITE).equals("undefined")) {
            waitForPseudo(Color.WHITE);
        }
    }
    
    private void waitForPseudo(Color color) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Configuration");
        dialog.setContentText("Enter a pseudo for " + color.name() + " player:");
        
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(name -> ContextHolder.getInstance().setPlayerName(color, name));
    }
    
    private void handleButtons() {
        playBtn.setOnAction((event) -> FxUtils.loadView("game", playBtn));
        customizeBtn.setOnAction((event) -> FxUtils.loadView("customize", playBtn));
        settingsBtn.setOnAction((event) -> FxUtils.loadView("settings", playBtn));
        leaderBoardBtn.setOnAction((event) -> displayComingSoonDialog());
        replayBtn.setOnAction((event) -> displayComingSoonDialog());
    }

    private void displayComingSoonDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Coming soon..");
        alert.setContentText("This feature is not available yet.");
        alert.show();

    }

}
