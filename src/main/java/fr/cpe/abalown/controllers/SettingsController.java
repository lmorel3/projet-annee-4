package fr.cpe.abalown.controllers;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.utils.FxUtils;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {

    @FXML
    Button backBtn;

    @FXML
    CheckBox fullScreen;

    @FXML
    CheckBox smallSized;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FxUtils.loadView("main", backBtn);
            }
        });

        fullScreen.setSelected(ContextHolder.getInstance().isFullScreen());
        smallSized.setSelected(ContextHolder.getInstance().isSmallSized());

        handleCheckboxes();

    }

    private void handleCheckboxes() {

        fullScreen.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                ContextHolder.getInstance().setFullScreen(newValue);

                Stage stage = (Stage) fullScreen.getScene().getWindow();
                stage.setFullScreen(newValue);
            }
        });

        smallSized.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                ContextHolder.getInstance().setSmallSized(newValue);

                FxUtils.loadView("settings", smallSized);
                // TODO: fix :FxUtils.centerStage((Stage) smallSized.getScene().getWindow());
            }
        });

    }

}

