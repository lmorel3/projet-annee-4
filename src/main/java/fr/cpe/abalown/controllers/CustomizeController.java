package fr.cpe.abalown.controllers;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.utils.FxUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

public class CustomizeController implements Initializable {

    @FXML
    Pane ballsContainer;

    @FXML
    Button backBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ballsContainer.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                EventTarget target = mouseEvent.getTarget();

                if (target instanceof ImageView) {
                    CustomizeController.this.handleSetBallAppearance(target);
                }

                System.out.println("Event " + target);

            }
        });

        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FxUtils.loadView("main", backBtn);
            }
        });

        resetSelection();

    }

    /**
     * Fired when user clicks on a ball appearance
     *
     * @param target
     */
    private void handleSetBallAppearance(EventTarget target) {
        ImageView image = ((ImageView) target);
        String appearance = image.getId();
        System.out.println("Selected appearance : " + appearance);

        ContextHolder.getInstance().setBallAppearance(appearance);
        ContextHolder.getInstance().setBoardAppearance(appearance);
        resetSelection();
    }

    private void resetSelection() {

        for (String ballAppearance : ContextHolder.getInstance().getAvailableBallAppearances()) {
            setAppearanceState(ballAppearance, false);
        }

        setAppearanceState(ContextHolder.getInstance().getBallAppearance(), true);

    }

    private void setAppearanceState(String appearance, boolean displayed) {
        ImageView image = (ImageView) ballsContainer.lookup("#" + appearance);

        if (image != null) {
            image.getParent().getChildrenUnmodifiable().get(0).setVisible(displayed);
        }
    }


}
