package fr.cpe.abalown.config;

/**
 * Default configuration values
 */
public class GameConfig {

    public static final double APP_WIDTH_LARGE = 1920;
    public static final double APP_HEIGHT_LARGE = 1080;

    public static final double APP_WIDTH_SMALL = 800;
    public static final double APP_HEIGHT_SMALL = 600;

    public static final double BOARD_WIDTH_LARGE = 800;
    public static final double BOARD_HEIGHT_LARGE = 800;

    public static final double BOARD_WIDTH_SMALL = 400;
    public static final double BOARD_HEIGHT_SMALL = 400;

    public static final double BALL_WIDTH_LARGE = 69;
    public static final double BALL_HEIGHT_LARGE = 59;

    public static final double BALL_WIDTH_SMALL = 35;
    public static final double BALL_HEIGHT_SMALL = 29.7;

}
