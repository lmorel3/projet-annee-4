package fr.cpe.abalown.config;


import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.utils.FileUtils;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ContextHolder {

    private static ContextHolder instance;

    private Map<Color, String> playersName = new ConcurrentHashMap<>();

    private List<String> availableBallAppearances;
    private String ballAppearance = "default";

    private List<String> availableBoardAppearances;
    private String boardAppearance = "default";

    private boolean isFullScreen = false;
    private boolean isSmallSized = true;

    private ContextHolder() {
        detectAvailableBallAppearances();
        detectAvailableBoardAppearances();
    }

    public static ContextHolder getInstance() {
        if (instance == null) {
            instance = new ContextHolder();
        }

        return instance;
    }

    public void setPlayerName(Color color, String name) {
        System.out.println(color.name() + "'s name: " + name);
        playersName.put(color, name);
    }

    public String getPlayerName(Color color) {
        return playersName.getOrDefault(color, "undefined");
    }

    private void detectAvailableBallAppearances() {
        availableBallAppearances = FileUtils.listFiles("/images/balls");
        System.out.println("availableBallAppearances = " + availableBallAppearances);
    }

    public List<String> getAvailableBallAppearances() {
        return availableBallAppearances;
    }

    public String getBallAppearance() {
        return ballAppearance;
    }

    public void setBallAppearance(String ballAppearance) {
        this.ballAppearance = ballAppearance;
    }

    private void detectAvailableBoardAppearances() {
        availableBoardAppearances = FileUtils.listFiles("/images/boards");
    }

    public List<String> getAvailableBoardAppearances() {
        return availableBoardAppearances;
    }

    public String getBoardAppearance() {
        return boardAppearance;
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        isFullScreen = fullScreen;
    }

    public boolean isSmallSized() {
        return isSmallSized;
    }

    public void setSmallSized(boolean smallSized) {
        isSmallSized = smallSized;
    }

    public double getBoardWidth() {
        return isSmallSized ? GameConfig.BOARD_WIDTH_SMALL : GameConfig.BOARD_WIDTH_LARGE;
    }

    public double getBoardHeight() {
        return isSmallSized ? GameConfig.BOARD_HEIGHT_SMALL : GameConfig.BOARD_HEIGHT_LARGE;
    }

    public double getBoardPosX() {
        return getAppWidth() / 2 - getBoardWidth() / 2;
    }

    public double getBoardPosY() {
        return getAppHeight() / 2 - getBoardHeight() / 2;
    }

    public double getAppWidth() {
        return isSmallSized ? GameConfig.APP_WIDTH_SMALL : GameConfig.APP_WIDTH_LARGE;
    }

    public double getAppHeight() {
        return isSmallSized ? GameConfig.APP_HEIGHT_SMALL : GameConfig.APP_HEIGHT_LARGE;
    }

    public double getBallWidth() {
        return isSmallSized ? GameConfig.BALL_WIDTH_SMALL : GameConfig.BALL_WIDTH_LARGE;
    }

    public double getBallHeight() {
        return isSmallSized ? GameConfig.BALL_HEIGHT_SMALL : GameConfig.BALL_HEIGHT_LARGE;
    }

    public void setBoardAppearance(String boardAppearance) {
        this.boardAppearance = boardAppearance;
    }
}
