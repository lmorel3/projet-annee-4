package fr.cpe.abalown.views;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;
import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.models.BoardCoord;
import fr.cpe.abalown.models.VisualCoord;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

import java.io.InputStream;

/**
 */
public class BallGui extends Pane {

    private ImageView imageView;
    private boolean isSelected = false;

    private BoardCoord boardCoord;
    private Color color;

    private BallGui() {
    }

    public BallGui(InputStream imageResource, BoardCoord boardCoord, Color color) {
        imageView = new ImageView(
            new Image(
                imageResource,
                ContextHolder.getInstance().getBallWidth(),
                ContextHolder.getInstance().getBallHeight(),
                true,
                true
            )
        );

        this.boardCoord = boardCoord;
        this.color = color;

        getChildren().add(imageView);
    }

    /**
     * Toggle selected state and highlight
     */
    public void toggleSelected() {
        isSelected = !isSelected;
        setStyle(isSelected ? "-fx-border-color: black" : "");
    }


    /**
     * Select the ball, highlight the cell
     */
    public void select() {
        isSelected = true;
        // highlight on
        setStyle("-fx-border-color: black; -fx-border-width: 3.0; -fx-border-radius: 100%");
    }

    /**
     * Unselect the ball, remove highlight
     */
    public void unSelect() {
        isSelected = false;
        // highlight off
        setStyle("");
    }

    public BoardCoord getBoardCoord() {
        return boardCoord;
    }

    public Color getColor() {
        return color;
    }
}
