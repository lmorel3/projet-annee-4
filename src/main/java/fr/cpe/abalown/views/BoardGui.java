package fr.cpe.abalown.views;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;
import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.factories.BallGuiFactory;
import fr.cpe.abalown.models.BoardBall;
import fr.cpe.abalown.models.BoardBalls;
import fr.cpe.abalown.models.BoardCoord;
import javafx.collections.ObservableList;
import javafx.scene.Node;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lmorel on 16/10/17.
 */
public class BoardGui extends Region {

    private ImageView boardImg;

    public BoardGui() {
        setCache(false);

        initBoard();
        getChildren().addAll(boardImg);
    }

    @Override
    public ObservableList<Node> getChildren() {
        return super.getChildren();
    }

    public void addBall(BallGui ballGui) {
        getChildren().add(ballGui);
    }

    public void addCell(CellGui cellGui) {
        getChildren().add(cellGui);
    }

    public void resetBalls() {
        ObservableList<Node> children = getChildren();
        List<Node> toRemove = new ArrayList<>();
        for (Node node : children) {
            if (node instanceof BallGui) {
                ((BallGui) node).unSelect();
                toRemove.add(node);
            }
        }

        children.removeAll(toRemove);
    }

    private void initBoard() {
        System.out.println("Initializing board");
        String imgPath = getCurrentBoardResourcePath();

        System.out.println("Board is " + imgPath);
        InputStream board = getClass().getResourceAsStream(imgPath);

        ContextHolder context = ContextHolder.getInstance();

        boardImg = new ImageView();
        boardImg.setImage(new Image(board, context.getBoardWidth(), context.getBoardHeight(), true, true));
        boardImg.setX(context.getBoardPosX());
        boardImg.setY(context.getBoardPosY());
    }

    public void highlightCell(BoardCoord coord) {
        ObservableList<Node> children = getChildren();
        for (Node node : children) {
            if (node instanceof CellGui) {
                CellGui cell = (CellGui) node;
                if (cell.getBoardCoord().equals(coord)) {
                    cell.highlightOn();
                }
            }
        }
    }

    public void unHighlightCell(BoardCoord coord) {
        ObservableList<Node> children = getChildren();
        for (Node node : children) {
            if (node instanceof CellGui) {
                CellGui cell = (CellGui) node;
                if (cell.getBoardCoord().equals(coord)) {
                    cell.highlightOff();
                }
            }
        }
    }

    private String getCurrentBoardResourcePath() {
        boolean isSmall = ContextHolder.getInstance().isSmallSized();
        String appearance = ContextHolder.getInstance().getBoardAppearance();

        String suffix = isSmall ? "small" : "large";

        return "/images/boards/" + appearance + "/board-" + suffix + ".png";
    }

}
