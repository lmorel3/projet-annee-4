package fr.cpe.abalown.views;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;
import fr.cpe.abalown.models.BoardCoord;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.io.InputStream;

/**
 */
public class CellGui extends Pane {

    private BoardCoord boardCoord;

    private CellGui() {}

    public CellGui(BoardCoord boardCoord) {
        this.boardCoord = boardCoord;
        this.setPrefSize(ContextHolder.getInstance().getBallWidth(), ContextHolder.getInstance().getBallHeight());
    }

    public BoardCoord getBoardCoord() {
        return boardCoord;
    }

    /**
     * highlight the cell
     */
    public void highlightOn() {
        setStyle("-fx-border-color: green; -fx-border-width: 3.0; -fx-border-radius: 100%");
    }

    /**
     * remove highlight
     */
    public void highlightOff() {
        setStyle("");
    }

    @Override
    public String toString() {
        return "CellGui @ " + boardCoord.getCol() + " ; " + boardCoord.getRow();
    }
}
