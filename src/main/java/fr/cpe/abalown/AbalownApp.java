package fr.cpe.abalown;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;

import fr.cpe.abalown.utils.FxUtils;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;


/**
 * Created by laurent on 04/10/17.
 */
public class AbalownApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("Loading game..");
        Parent root = FxUtils.getView("main");

        Scene scene = new Scene(root, FxUtils.getWindowWidth(), FxUtils.getWindowHeight());
        stage.setWidth(FxUtils.getWindowWidth());
        stage.setHeight(FxUtils.getWindowHeight());

        stage.setTitle("Abalown");
        stage.setScene(scene);
        stage.setResizable(true);

        if (!ContextHolder.getInstance().isSmallSized()) {
            ContextHolder.getInstance().setFullScreen(true);
            stage.setFullScreen(ContextHolder.getInstance().isFullScreen());
        }

        FxUtils.centerStage(stage);
        stage.show();

    }

}
