package fr.cpe.abalown.models;

/**
 * Coordonnées en pixels
 */
public class VisualCoord {

    private int x;
    private int y;

    public VisualCoord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean equals(Object o) {
        if (o instanceof VisualCoord) {
            VisualCoord coord = (VisualCoord) o;
            if (coord.getX() == this.x && coord.getY() == this.y) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return "VisualCoord{x=" + this.getX() + ", y=" + this.getY() + "}";
    }
}
