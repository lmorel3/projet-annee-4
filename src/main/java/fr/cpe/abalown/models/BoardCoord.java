package fr.cpe.abalown.models;

public class BoardCoord implements Comparable<BoardCoord> {

    private Integer row;

    private Integer col;

    public BoardCoord(Integer row, Integer col) {
        this.row = row;
        this.col = col;
    }

    public BoardCoord(BoardCoord other) {
        this.row = other.row;
        this.col = other.col;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getCol() {
        return col;
    }

    public void setCol(Integer col) {
        this.col = col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)  {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoardCoord coord = (BoardCoord) o;

        if (row != null ? !row.equals(coord.row) : coord.row != null) {
            return false;
        }

        return col != null ? col.equals(coord.col) : coord.col == null;
    }

    @Override
    public int hashCode() {
        int result = row != null ? row.hashCode() : 0;
        result = 31 * result + (col != null ? col.hashCode() : 0);

        return result;
    }

    @Override
    public int compareTo(BoardCoord o) {
        return 10 * this.row - o.row + this.col - o.col;
    }

    @Override
    public String toString() {
        return "BoardCoord{"
                + "row=" + row
                + ", col=" + col
                + '}';
    }
}
