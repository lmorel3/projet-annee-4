package fr.cpe.abalown.models;

import fr.cpe.abalown.enums.Color;

public class Player {

    private BoardBalls boardBalls;
    private Color color;

    public Player(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public BoardBalls getBoardBalls() {
        return boardBalls;
    }

    public void setBoardBalls(BoardBalls boardBalls) {
        this.boardBalls = boardBalls;
    }
}
