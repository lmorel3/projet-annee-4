package fr.cpe.abalown.models;

import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.enums.Direction;
import fr.cpe.abalown.events.EjectionEventData;
import fr.cpe.abalown.utils.InitCoordProvider;
import fr.cpe.abalown.utils.DrawGameToConsole;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

public class Game extends Observable {

    private Player blackPlayer;
    private Player whitePlayer;

    private GameMoveChecker gameMoveChecker;

    private BoardCoord ejectedBall;

    public Game() {
        blackPlayer = new Player(Color.BLACK);
        whitePlayer = new Player(Color.WHITE);

        blackPlayer.setBoardBalls(new BoardBalls(InitCoordProvider.getBlackInitCoord()));
        whitePlayer.setBoardBalls(new BoardBalls(InitCoordProvider.getWhiteInitCoord()));

        gameMoveChecker = new GameMoveChecker(
                blackPlayer.getBoardBalls().getBalls(),
                whitePlayer.getBoardBalls().getBalls()
        );
    }

    public BoardBalls getPlayerBoardBalls(Color color) {
        if (color == Color.BLACK) {
            return blackPlayer.getBoardBalls();
        } else if (color == Color.WHITE) {
            return whitePlayer.getBoardBalls();
        }

        return null;
    }

    /**
     * * Indicates the color of the ball (if exists) at the given coordinates
     *
     * @param coord the coordinates to check
     * @return the Color of the ball or null if there is no ball
     */
    public Color whatColorIsThisBall(BoardCoord coord) {
        if (blackPlayer.getBoardBalls().getBalls().containsKey(coord)) {
            return Color.BLACK;
        } else if (whitePlayer.getBoardBalls().getBalls().containsKey(coord)) {
            return Color.WHITE;
        } else {
            return null;
        }
    }

    /**
     * Determines in which direction a move is made.
     * The move MUST be a valid move :
     * selectedBalls are aligned and adjacent
     * <p>
     * This method has no effect on the position of any ball.
     *
     * @param selectedBalls the selected balls
     * @param target        the targeted cell
     * @return Direction The direction corresponding to the move
     * or null if the move is invalid
     */
    public Direction determineDirection(List<BoardCoord> selectedBalls, BoardCoord target) {

        boolean targetIsNeighborOfSelectedBalls = false;
        for (BoardCoord selectedBall : selectedBalls) {
            if (gameMoveChecker.isNeighbor(selectedBall, target)) {
                targetIsNeighborOfSelectedBalls = true;
            }
        }

        if (!targetIsNeighborOfSelectedBalls) {
            return null;
        }

        int nbBalls = selectedBalls.size();

        if (nbBalls == 1) {
            // ONE BALL
            BoardCoord ball = selectedBalls.get(0);
            if (target.getRow() > ball.getRow()) {

                // up
                if (target.getCol().equals(ball.getCol())) {
                    // UP-LEFT
                    return Direction.UPLEFT;
                } else if (target.getCol().equals(ball.getCol() + 1)) {
                    // UP-RIGHT
                    return Direction.UPRIGHT;
                } else {
                    // error (invalid move)
                    return null;
                }

            } else if (target.getRow().equals(ball.getRow())) {
                // horizontal move

                if (target.getCol().equals(ball.getCol() - 1)) {
                    return Direction.LEFT;
                } else if (target.getCol().equals(ball.getCol() + 1)) {
                    return Direction.RIGHT;
                } else {
                    // error (invalid move)
                    return null;
                }

            } else {
                // down

                if (target.getCol().equals(ball.getCol() - 1)) {
                    // DOWN-LEFT
                    return Direction.DOWNLEFT;
                } else if (target.getCol().equals(ball.getCol())) {
                    // DOWN-RIGHT
                    return Direction.DOWNRIGHT;
                } else {
                    // error (invalid move)
                    return null;
                }
            }
        } else {
            // 2 or 3 BALLS

            BoardCoord ball0;
            BoardCoord ball1;
            BoardCoord ball2;

            ball0 = selectedBalls.get(0);
            ball1 = selectedBalls.get(1);
            ball2 = selectedBalls.get(1);

            if (nbBalls == 3) {
                ball2 = selectedBalls.get(2);
            }

            if (ball0.getRow() == ball1.getRow()) {
                if (nbBalls == 2 || ball1.getRow() == ball2.getRow()) {
                    // the balls are horizontally aligned

                    // Sort the balls by column
                    //  -> the first ball is to the left, the last one is to the right
                    Collections.sort(selectedBalls, (b1, b2) -> b1.getCol() < b2.getCol() ? -1 : 1);

                    // get left ball (the first one)
                    BoardCoord leftBall = selectedBalls.get(0);

                    // check left side
                    if (target.equals(getCoordOfThisDirection(leftBall, Direction.LEFT))) {
                        return Direction.LEFT;
                    } else if (target.equals(getCoordOfThisDirection(leftBall, Direction.UPLEFT))) {
                        return Direction.UPLEFT;
                    } else if (target.equals(getCoordOfThisDirection(leftBall, Direction.DOWNLEFT))) {
                        return Direction.DOWNLEFT;
                    }

                    // get right ball (the last one)
                    BoardCoord rightBall = selectedBalls.get(nbBalls - 1);
                    // check right side
                    if (target.equals(getCoordOfThisDirection(rightBall, Direction.RIGHT))) {
                        return Direction.RIGHT;
                    } else if (target.equals(getCoordOfThisDirection(rightBall, Direction.UPRIGHT))) {
                        return Direction.UPRIGHT;
                    } else if (target.equals(getCoordOfThisDirection(rightBall, Direction.DOWNRIGHT))) {
                        return Direction.DOWNRIGHT;
                    }

                    // here, the balls are horizontally aligned but target doesn't match a valid move
                    return null;
                }
            }

            // here, the balls are not horizontally aligned

            Collections.sort(selectedBalls, (b1, b2) -> b1.getRow() < b2.getRow() ? -1 : 1);
            BoardCoord lowestBall = selectedBalls.get(0);
            BoardCoord highestBall = selectedBalls.get(nbBalls - 1);
            System.out.println("lowest:" + lowestBall);
            System.out.println("highest" + highestBall);

            if (ball0.getCol() == ball1.getCol()) {
                if (nbBalls == 2
                        || ball1.getCol() == ball2.getCol()) {
                    // balls are aligned along UPLEFT-DOWNRIGHT axis
                    System.out.println("UPLEFT-DOWNRIGHT axis");

                    if (target.equals(getCoordOfThisDirection(highestBall, Direction.LEFT))) {
                        return Direction.LEFT;
                    } else if (target.equals(getCoordOfThisDirection(highestBall, Direction.UPLEFT))) {
                        return Direction.UPLEFT;
                    } else if (target.equals(getCoordOfThisDirection(highestBall, Direction.UPRIGHT))) {
                        return Direction.UPRIGHT;
                    } else if (target.equals(getCoordOfThisDirection(lowestBall, Direction.RIGHT))) {
                        return Direction.RIGHT;
                    } else if (target.equals(getCoordOfThisDirection(lowestBall, Direction.DOWNRIGHT))) {
                        return Direction.DOWNRIGHT;
                    } else if (target.equals(getCoordOfThisDirection(lowestBall, Direction.DOWNLEFT))) {
                        return Direction.DOWNLEFT;
                    }

                }
            }

            BoardCoord middleBall = nbBalls == 3 ? selectedBalls.get(1) : null;
            if ((nbBalls == 2 && highestBall.equals(getCoordOfThisDirection(lowestBall, Direction.UPRIGHT)))
                    || (nbBalls == 3 && highestBall.equals(getCoordOfThisDirection(middleBall, Direction.UPRIGHT))
                    && middleBall.equals(getCoordOfThisDirection(lowestBall, Direction.UPRIGHT)))) {

                // balls are aligned along DOWNLEFT-UPRIGHT axis
                System.out.println("DOWNLEFT-UPRIGHT axis");

                if (target.equals(getCoordOfThisDirection(highestBall, Direction.UPLEFT))) {
                    return Direction.UPLEFT;
                } else if (target.equals(getCoordOfThisDirection(highestBall, Direction.UPRIGHT))) {
                    return Direction.UPRIGHT;
                } else if (target.equals(getCoordOfThisDirection(highestBall, Direction.RIGHT))) {
                    return Direction.RIGHT;
                } else if (target.equals(getCoordOfThisDirection(lowestBall, Direction.DOWNRIGHT))) {
                    return Direction.DOWNRIGHT;
                } else if (target.equals(getCoordOfThisDirection(lowestBall, Direction.DOWNLEFT))) {
                    return Direction.DOWNLEFT;
                } else if (target.equals(getCoordOfThisDirection(lowestBall, Direction.LEFT))) {
                    return Direction.LEFT;
                }
            }

        }

        return null;
    }

    /**
     * Determines if a move is authorized or not.
     * Does not move any ball.
     *
     * @param balls     the 1, 2 or 3 balls which are wanted to be moved
     * @param direction the direction to move the balls to
     * @return true if the move is allowed, false if not
     */
    public boolean canMove(List<BoardCoord> balls, Direction direction) {

        if (balls == null || direction == null) {
            return false;
        }

        if (balls.isEmpty() || balls.size() > 3) {
            return false;
        }

        BoardCoord targetOne = null;
        BoardCoord targetTwo = null;
        BoardCoord targetThree = null;

        Color playerColor = whatColorIsThisBall(balls.get(0));

        if (balls.size() == 1) {
            // One ball
            BoardCoord ball = balls.get(0);

            // for one ball, we know there is exactly one target
            targetOne = getTargetsOfMove(balls, direction).get(0);

            if (!gameMoveChecker.isCoordOnBoard(targetOne)) {
                // outside the board  : move is illegal with own balls
                System.out.println("Target is outside the board");
                return false;
            }

            if (gameMoveChecker.isBallHere(targetOne)) {
                // il y a une boule dans la direction où on veut aller
                System.out.println("Ball on target");
                // déplacement non autorisé car on ne bouge qu'une seule boule, il n'y a pas de sumito
                return false;
            } else {
                // on arrive sur une case vide
                System.out.println("No ball on target");
                // case vide : mouvement autorisé, car on ne bouge qu'une seule boule
                return true;
            }
        } else {
            // plusieurs boules

            // on récupère les cases d'arrivée des boules
            List<BoardCoord> targets = getTargetsOfMove(balls, direction);
            System.out.println(targets.size() + " targets  : " + targets.toArray());

            if (targets.size() == 1) {
                // on vise une seule case (mouvement dans l'alignement) -> sumito éventuel

                // on regarde la boule visée
                targetOne = targets.get(0);
                System.out.println("targetOne : " + targetOne);
                if (gameMoveChecker.isBallHere(targetOne)) {
                    if (whatColorIsThisBall(targetOne).equals(playerColor)) {
                        // boule de la même couleur : mouvement invalide
                        System.out.println("no-1");
                        return false;
                    } else {
                        // on essaie de pousser une première boule adverse

                        // on vérifie la deuxième case
                        targetTwo = getCoordOfThisDirection(targetOne, direction);
                        System.out.println("targetTwo :" + targetTwo);
                        if (!gameMoveChecker.isCoordOnBoard(targetTwo)) {
                            // on pousse en dehors du plateau
                            // sumito valide + éjection
                            System.out.println("yes-2");
                            return true;
                        } else if (gameMoveChecker.isBallHere(targetTwo)) {
                            // il y a une deuxième boule
                            if (whatColorIsThisBall(targetTwo).equals(playerColor)) {
                                // la case d'après est une boule du joueur courant
                                // sumito invalide, mouvement non autorisé
                                System.out.println("no-3");
                                return false;
                            } else {
                                // la case d'après est une boule adverse.
                                // il peut y avoir sumito en poussant 2 boules

                                // on regarde alors la troisième case
                                targetThree = getCoordOfThisDirection(targetTwo, direction);
                                System.out.println("targetThree :" + targetThree);

                                if (!gameMoveChecker.isCoordOnBoard(targetThree)) {
                                    // on pousse la deuxième boule en dehors du plateau
                                    // sumito valide + éjection
                                    System.out.println("yes-4");
                                    return true;
                                } else if (gameMoveChecker.isBallHere(targetThree)) {
                                    // il y a une boule (quelconque) qui bloque les deux autres :
                                    // sumito impossible
                                    System.out.println("no-5");
                                    return false;
                                } else {
                                    // pas de troisième boule
                                    // on essaie de pousser deux boules adverses avec une case libre après
                                    if (balls.size() > 2) {
                                        // on assez de boules pour pousser
                                        //  ==> SUMITO !
                                        System.out.println("Sumito !");
                                        System.out.println("yes-6");
                                        return true;
                                    } else {
                                        // pas assez de boules pour pousser
                                        System.out.println("no-7");
                                        return false;
                                    }
                                }
                            }
                        } else {
                            // on essaie de pousser 1 boule adverse avec une case libre derrière
                            if (balls.size() > 1) {
                                // on a assez de boules pour pousser
                                //  ==> SUMITO !
                                System.out.println("Sumito !");
                                System.out.println("yes-8");
                                return true;
                            } else {
                                // pas assez de boules pour pousser
                                System.out.println("no-9");
                                return false;
                            }
                        }

                    }
                } else {
                    // la case targetOne est libre
                    //  mouvement de 2/3 cases dans l'alignement, vers une case vide
                    //  -> mouvement autorisé
                    return true;
                }
            } else {
                // on vise plusieurs cases en même temps
                //  -> mouvement latéral de deux ou trois boules
                //  -> pas de sumito possible avec ce type de mouvement
                System.out.println("Lateral move");

                // on vérifie que chaque boule arrive sur une case libre et dans le plateau
                for (BoardCoord target : targets) {
                    if (!gameMoveChecker.isCoordOnBoard(target)) {
                        System.out.println("Target " + target + " is outside the board");
                        return false;
                    }

                    if (gameMoveChecker.isBallHere(target)) {
                        // il y a une boule qui bloque
                        System.out.println("Ball on target");
                        return false;
                    }
                }
                // si on arrive ici c'est que le mouvement est autorisé
                return true;
            }
        }
    }

    /**
     * Obtains the targeted coords of the cell(s) where the balls would go if the move was made
     * If the move is in the same direction as the aligned balls, target will be only one cell.
     *
     * @param balls
     * @param direction
     */
    public List<BoardCoord> getTargetsOfMove(List<BoardCoord> balls, Direction direction) {

        List<BoardCoord> targets = new ArrayList<>();
        BoardCoord target = null;

        for (BoardCoord ball : balls) {
            // pour chaque boule, on retient où elle va
            target = getCoordOfThisDirection(ball, direction);

            targets.add(target);
        }

        // si certaines cases cibles sont aussi des cases de départ (cas du mouvement dans l'alignement),
        //  on ne les prend pas en compte
        targets.removeAll(balls);

        System.out.println("Targets : " + targets);

        return targets;
    }

    /**
     * Obtain the coordinates of the cell which is in the given direction, from the given origin.
     *
     * @param origin
     * @param direction
     * @return BoardCoord Coordinates of the target,
     * or null if target is outside the board or if origin is null
     */
    public BoardCoord getCoordOfThisDirection(BoardCoord origin, Direction direction) {

        if (origin == null) {
            return null;
        }

        BoardCoord target = null;

        switch (direction) {
            case UPLEFT:
                target = new BoardCoord(origin.getRow() + 1, origin.getCol() + 0);
                break;
            case UPRIGHT:
                target = new BoardCoord(origin.getRow() + 1, origin.getCol() + 1);
                break;
            case LEFT:
                target = new BoardCoord(origin.getRow() + 0, origin.getCol() - 1);
                break;
            case RIGHT:
                target = new BoardCoord(origin.getRow() + 0, origin.getCol() + 1);
                break;
            case DOWNLEFT:
                target = new BoardCoord(origin.getRow() - 1, origin.getCol() - 1);
                break;
            case DOWNRIGHT:
                target = new BoardCoord(origin.getRow() - 1, origin.getCol() + 0);
                break;
            default:
                break;
        }

        if (!gameMoveChecker.isCoordOnBoard(target)) {
            // en dehors du plateau
            return null;
        }

        return target;
    }

    /**
     * Get the list of enemy balls that are pushed by the player's balls
     * The corresponding move MUST be a sumito.
     *
     * @param balls     the player's balls that are moving
     * @param direction the direction of the move
     * @return
     */
    public List<BoardCoord> getBallsPushedByThisMove(List<BoardCoord> balls, Direction direction) {
        List<BoardCoord> pushedBalls = new ArrayList<>();

        // on prend la première boule poussée
        BoardCoord pushedBall = getTargetsOfMove(balls, direction).get(0);
        pushedBalls.add(pushedBall);

        // on regarde s'il y en a une autre
        BoardCoord ball = getCoordOfThisDirection(pushedBall, direction);
        if (gameMoveChecker.isBallHere(ball)) {
            pushedBalls.add(ball);
        }
        return pushedBalls;
    }

    /**
     * Moves a single ball.
     *
     * @param ball      the ball that is wanted to be moved
     * @param direction the direction
     * @return
     */
    private List<BoardCoord> doMove(BoardCoord ball, Direction direction, List<BoardCoord> movedBalls) {

        BoardCoord target = getCoordOfThisDirection(ball, direction);

        if (gameMoveChecker.isBallHere(target)) {
            // target is another ball, which has not been moved yet
            // recursion : move the other balls before the current ball
            System.out.println("Ball " + target + " is on target of " + ball + " -> moving it");
            List<BoardCoord> m = doMove(target, direction, movedBalls);
            //movedBalls.addAll(m);
        }

        // at this point, we can move the ball

        if (!gameMoveChecker.isCoordOnBoard(target)) {
            // EJECTION !
            ejectedBall = ball;

            this.setChanged();

            Color color = whatColorIsThisBall(ejectedBall);
            int remainingBalls = (color.equals(Color.BLACK))
                    ? blackPlayer.getBoardBalls().count()
                    : whitePlayer.getBoardBalls().count();
            EjectionEventData data = new EjectionEventData(color, remainingBalls - 1); // -1 because remove later

            this.notifyObservers(data);

            whitePlayer.getBoardBalls().removeBallFromBoard(ball);
            blackPlayer.getBoardBalls().removeBallFromBoard(ball);
        } else {

            // if the ball does not belongs to a player, it belongs to the other
            whitePlayer.getBoardBalls().doMoveBall(ball, target);
            blackPlayer.getBoardBalls().doMoveBall(ball, target);
        }

        movedBalls.add(ball);

        return movedBalls;
    }

    /**
     * Moves a list of balls.
     * In the case of a sumito, the balls list must contain the pushed ball.
     * The move must be a valid move.
     *
     * @param ballsToMove the list of balls to move
     * @param direction   the direction
     * @return the balls that were moved
     */
    public List<BoardCoord> doMove(List<BoardCoord> ballsToMove, Direction direction) {

        List<BoardCoord> movedBalls = new ArrayList<>();

        for (BoardCoord ball : ballsToMove) {

            if (!movedBalls.contains(ball)) {
                // this ball has not been moved yet


                // where does this ball wants to go ?
                BoardCoord target = getCoordOfThisDirection(ball, direction);

                if (!gameMoveChecker.isCoordOnBoard(target)) {
                    // ejected ball ! (sumito)
                    // trigger event ?
                }

                //if (ballsToMove.contains(target) && !movedBalls.contains(target)) {
                // target is another ball, which has not been moved yet
                // recursion : move the other balls before the current ball
//                    List<BoardCoord> otherBalls = new ArrayList<>(ballsToMove);
//                    otherBalls.remove(ball);
//                    System.out.println("Ball " + target + " is on target of " + ball + " -> moving it");
//                    List<BoardCoord> m = doMove(otherBalls, direction);
//                    movedBalls.addAll(m);
                //}

                System.out.println("Moving ball " + ball + " to " + target);
                // do move this particular ball
                doMove(ball, direction, movedBalls);
                // this ball has been moved now
                //movedBalls.add(ball);
            }
        }
        return movedBalls;
    }

    public BoardCoord getEjectedBall() {
        return ejectedBall;
    }

    @Override
    public String toString() {
        return DrawGameToConsole.drawArrayToConsole(this) + "\n\n" + DrawGameToConsole.drawHexagonToConsole(this);
    }
}
