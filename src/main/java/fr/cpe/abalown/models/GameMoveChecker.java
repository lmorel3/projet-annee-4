package fr.cpe.abalown.models;

import fr.cpe.abalown.enums.Color;

import java.util.Map;

public class GameMoveChecker {

    private Map<BoardCoord, BoardBall> blackBoardBalls;
    private Map<BoardCoord, BoardBall> whiteBoardBalls;

    public GameMoveChecker(Map<BoardCoord, BoardBall> blackBoardBalls, Map<BoardCoord, BoardBall> whiteBoardBalls) {
        this.blackBoardBalls = blackBoardBalls;
        this.whiteBoardBalls = whiteBoardBalls;
    }

    public Boolean isCoordOnBoard(BoardCoord coord) {
        if (coord == null) {
            return false;
        }

        int startOffset = 4;
        int endOffset = 8;

        for (int i = 8; i >= 0; i--) {
            for (int j = 0; j < 9; j++) {
                if (i == coord.getCol() && j == coord.getRow()) {
                    if (j >= startOffset && j <= endOffset) {
                        return true;
                    }
                }
            }
            if (i > 4) {
                startOffset--;
            } else {
                endOffset--;
            }
        }

        return false;
    }

    /**
     * Indicates if there is a ball belonging to this player
     *
     * @param player 
     * @param coord
     * @return
     */
    public Boolean isPlayerBallHere(Player player, BoardCoord coord) {
        if (player == null || coord == null) {
            return false;
        }

        if (player.getColor() == Color.BLACK) {
            return blackBoardBalls.containsKey(coord);
        } else {
            return whiteBoardBalls.containsKey(coord);
        }
    }

    /**
     * Indicates if a ball exists at this location
     *
     * @param coord The coords of the location to test
     * @return true if there is a ball, false if not
     */
    public Boolean isBallHere(BoardCoord coord) {
        return blackBoardBalls.containsKey(coord) || whiteBoardBalls.containsKey(coord);
    }

    /**
     * Indicates if ballTwo is a neighbor of ballOne.
     *
     * @param ballOne the first ball
     * @param ballTwo the other ball
     * @return true if ballTwo is a neighbor of ballOne, false if not
     */
    public Boolean isNeighbor(BoardCoord ballOne, BoardCoord ballTwo) {

        if (ballOne == null || ballTwo == null) {
            return false;
        }

        if (ballOne.getRow() == ballTwo.getRow()
                && ballOne.getCol() == ballTwo.getCol()) {
            // that's the same ball -> not neighbor
            return false;
        }

        // ballTwo in the row below
        if (ballTwo.getRow() == ballOne.getRow() - 1) {
            if (ballTwo.getCol() == ballOne.getCol()
                    || ballTwo.getCol() == ballOne.getCol() - 1) {
                return true;
            }
        }

        // ballTwo in the same row
        if (ballTwo.getRow() == ballOne.getRow()) {
            if (Math.abs(ballTwo.getCol() - ballOne.getCol()) == 1) {
                return true;
            }
        }

        // ballTwo in the row above
        if (ballTwo.getRow() == ballOne.getRow() + 1) {
            if (ballTwo.getCol() == ballOne.getCol()
                    || ballTwo.getCol() == ballOne.getCol() + 1) {
                return true;
            }
        }

        return false;
    }
}
