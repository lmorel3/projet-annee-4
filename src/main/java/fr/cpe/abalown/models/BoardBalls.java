package fr.cpe.abalown.models;

import java.util.Map;

public class BoardBalls {

    private Map<BoardCoord, BoardBall> balls;

    public BoardBalls(Map<BoardCoord, BoardBall> balls) {
        this.balls = balls;
    }

    public Map<BoardCoord, BoardBall> getBalls() {
        return balls;
    }

    public void setBalls(Map<BoardCoord, BoardBall> balls) {
        this.balls = balls;
    }

    public boolean doMoveBall(BoardCoord origin, BoardCoord dest) {
        BoardBall ball = balls.remove(origin);
        if (ball == null) {
            return false;
        }
        balls.put(dest, ball);
        System.out.println("doMoveBall : BoardBall(" + ball + ") from " + origin + " to " + dest);
        return true;
    }

    public boolean removeBallFromBoard(BoardCoord ejected) {
        BoardBall ball = balls.remove(ejected);
        if (ball == null) {
            return false;
        }
        System.out.println("Ejected ball from " + ejected);
        return true;
    }

    public int count() {
        return balls.size();
    }

}
