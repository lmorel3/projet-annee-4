package fr.cpe.abalown.factories;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;
import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.models.BoardCoord;
import fr.cpe.abalown.models.VisualCoord;
import fr.cpe.abalown.utils.CoordConverter;
import fr.cpe.abalown.views.BallGui;

import java.io.InputStream;

/**
 */
public class BallGuiFactory {

    CoordConverter coordConverter = new CoordConverter();

    public BallGui createBall(Color color, BoardCoord coord) {
        VisualCoord visualCoord = coordConverter.getVisualCoordFromBoardCoord(coord.getCol(), coord.getRow());

        InputStream img = getImageResource(color);

        BallGui ball = new BallGui(img, coord, color);
        ball.setLayoutX(visualCoord.getX() + ContextHolder.getInstance().getBoardPosX());
        ball.setLayoutY(visualCoord.getY() + ContextHolder.getInstance().getBoardPosY());

        return ball;
    }

    private InputStream getImageResource(Color color) {
        String dir = "/images/balls/" + ContextHolder.getInstance().getBallAppearance();

        String small = ContextHolder.getInstance().isSmallSized() ? "-small" : "";

        if (color.equals(Color.WHITE)) {
            return getClass().getResourceAsStream(dir + "/white-ball" + small + ".png");
        }

        return getClass().getResourceAsStream(dir + "/black-ball" + small + ".png");
    }

}
