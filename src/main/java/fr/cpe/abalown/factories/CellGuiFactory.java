package fr.cpe.abalown.factories;

import fr.cpe.abalown.config.ContextHolder;
import fr.cpe.abalown.config.GameConfig;
import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.models.BoardCoord;
import fr.cpe.abalown.models.VisualCoord;
import fr.cpe.abalown.utils.CoordConverter;
import fr.cpe.abalown.views.BallGui;
import fr.cpe.abalown.views.CellGui;

import java.io.InputStream;

/**
 */
public class CellGuiFactory {

    CoordConverter coordConverter = new CoordConverter();

    public CellGui createCell(BoardCoord coord) {
        VisualCoord visualCoord = coordConverter.getVisualCoordFromBoardCoord(coord.getCol(), coord.getRow());

        CellGui cellGui = new CellGui(coord);
        cellGui.setLayoutX(visualCoord.getX() + ContextHolder.getInstance().getBoardPosX());
        cellGui.setLayoutY(visualCoord.getY() + ContextHolder.getInstance().getBoardPosY());

        return cellGui;
    }

}
