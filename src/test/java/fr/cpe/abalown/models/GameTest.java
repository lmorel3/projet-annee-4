package fr.cpe.abalown.models;

import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.enums.Direction;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GameTest {

    @Test
    public void testDrawToConsole() {
        Game game = new Game();
        System.out.println(game);
    }

    @Test
    public void testDetermineDirection() {

        Game game = new Game();

        // ONE BALL
        List<BoardCoord> balls = new ArrayList<>();
        BoardCoord target;

        balls.add(new BoardCoord(4, 3));

        // RIGHT
        target = new BoardCoord(4, 4);
        Assert.assertEquals(Direction.RIGHT, game.determineDirection(balls, target));

        // LEFT
        target = new BoardCoord(4, 2);
        Assert.assertEquals(Direction.LEFT, game.determineDirection(balls, target));

        // UPRIGHT
        target = new BoardCoord(5, 4);
        Assert.assertEquals(Direction.UPRIGHT, game.determineDirection(balls, target));

        // UPLEFT
        target = new BoardCoord(5, 3);
        Assert.assertEquals(Direction.UPLEFT, game.determineDirection(balls, target));

        // DOWNRIGHT
        target = new BoardCoord(3, 3);
        Assert.assertEquals(Direction.DOWNRIGHT, game.determineDirection(balls, target));

        // DOWNLEFT
        target = new BoardCoord(3, 2);
        Assert.assertEquals(Direction.DOWNLEFT, game.determineDirection(balls, target));


        // TWO BALLS

        // horizontal balls
        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 3));
        balls.add(new BoardCoord(4, 2));

        // RIGHT
        target = new BoardCoord(4, 4);
        Assert.assertEquals(Direction.RIGHT, game.determineDirection(balls, target));

        // LEFT
        target = new BoardCoord(4, 1);
        Assert.assertEquals(Direction.LEFT, game.determineDirection(balls, target));

        // UPRIGHT
        target = new BoardCoord(5, 4);
        Assert.assertEquals(Direction.UPRIGHT, game.determineDirection(balls, target));

        // UPLEFT
        target = new BoardCoord(5, 2);
        Assert.assertEquals(Direction.UPLEFT, game.determineDirection(balls, target));

        // DOWNRIGHT
        target = new BoardCoord(3, 3);
        Assert.assertEquals(Direction.DOWNRIGHT, game.determineDirection(balls, target));

        // DOWNLEFT
        target = new BoardCoord(3, 1);
        Assert.assertEquals(Direction.DOWNLEFT, game.determineDirection(balls, target));


        // 2 balls aligned along UPLEFT-DOWNRIGHT axis
        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 3));
        balls.add(new BoardCoord(5, 3));

        target = new BoardCoord(6, 4);
        Assert.assertEquals(Direction.UPRIGHT, game.determineDirection(balls, target));

        target = new BoardCoord(4, 4);
        Assert.assertEquals(Direction.RIGHT, game.determineDirection(balls, target));

        target = new BoardCoord(3, 3);
        Assert.assertEquals(Direction.DOWNRIGHT, game.determineDirection(balls, target));

        target = new BoardCoord(3, 2);
        Assert.assertEquals(Direction.DOWNLEFT, game.determineDirection(balls, target));

        target = new BoardCoord(5, 2);
        Assert.assertEquals(Direction.LEFT, game.determineDirection(balls, target));

        target = new BoardCoord(6, 3);
        Assert.assertEquals(Direction.UPLEFT, game.determineDirection(balls, target));


        target = new BoardCoord(5, 4);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        target = new BoardCoord(4, 2);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        target = new BoardCoord(3, 4);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        target = new BoardCoord(3, 1);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        target = new BoardCoord(6, 2);
        Assert.assertEquals(null, game.determineDirection(balls, target));


        // 2 balls aligned along DOWNLEFT-UPRIGHT axis
        balls = new ArrayList<>();
        balls.add(new BoardCoord(5, 3));
        balls.add(new BoardCoord(4, 2));


        target = new BoardCoord(6, 4);
        Assert.assertEquals(Direction.UPRIGHT, game.determineDirection(balls, target));

        target = new BoardCoord(5, 4);
        Assert.assertEquals(Direction.RIGHT, game.determineDirection(balls, target));

        target = new BoardCoord(3, 2);
        Assert.assertEquals(Direction.DOWNRIGHT, game.determineDirection(balls, target));

        target = new BoardCoord(3, 1);
        Assert.assertEquals(Direction.DOWNLEFT, game.determineDirection(balls, target));

        target = new BoardCoord(4, 1);
        Assert.assertEquals(Direction.LEFT, game.determineDirection(balls, target));

        target = new BoardCoord(6, 3);
        Assert.assertEquals(Direction.UPLEFT, game.determineDirection(balls, target));


        target = new BoardCoord(4, 3);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        target = new BoardCoord(5, 2);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        target = new BoardCoord(3, 0);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        target = new BoardCoord(5, 1);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        target = new BoardCoord(6, 2);
        Assert.assertEquals(null, game.determineDirection(balls, target));

        // THREE BALLS
    }


    @Test
    public void testGetCoordOfThisDirection() {

        Game game = new Game();

        BoardCoord origin = new BoardCoord(5, 3);
        BoardCoord cell = null;


        // UPLEFT
        cell = new BoardCoord(6, 3);
        Assert.assertEquals(cell, game.getCoordOfThisDirection(origin, Direction.UPLEFT));

        // UPRIGHT
        cell = new BoardCoord(6, 4);
        Assert.assertEquals(cell, game.getCoordOfThisDirection(origin, Direction.UPRIGHT));

        // RIGHT
        cell = new BoardCoord(5, 4);
        Assert.assertEquals(cell, game.getCoordOfThisDirection(origin, Direction.RIGHT));

        // DOWNRIGHT
        cell = new BoardCoord(4, 3);
        Assert.assertEquals(cell, game.getCoordOfThisDirection(origin, Direction.DOWNRIGHT));

        // DOWNLEFT
        cell = new BoardCoord(4, 2);
        Assert.assertEquals(cell, game.getCoordOfThisDirection(origin, Direction.DOWNLEFT));

        // LEFT
        cell = new BoardCoord(5, 2);
        Assert.assertEquals(cell, game.getCoordOfThisDirection(origin, Direction.LEFT));


        // en dehors du plateau
        origin = new BoardCoord(2, 0);
        Assert.assertNull(game.getCoordOfThisDirection(origin, Direction.DOWNLEFT));
        Assert.assertNull(game.getCoordOfThisDirection(origin, Direction.LEFT));

    }

    @Test
    public void testGetTargetsOfMove() {

        Game game = new Game();


        List<BoardCoord> balls = new ArrayList<>();
        Direction direction = null;
        List<BoardCoord> targets = null;
        List<BoardCoord> expected = null;

        // One ball
        balls.add(new BoardCoord(4, 3));


        targets = game.getTargetsOfMove(balls, Direction.UPLEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(5, 3));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.UPRIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(5, 4));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.RIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(4, 4));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.DOWNRIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(3, 3));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.DOWNLEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(3, 2));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.LEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(4, 2));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        // Two balls

        // aligned horizontally
        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 3));
        balls.add(new BoardCoord(4, 4));

        targets = game.getTargetsOfMove(balls, Direction.UPLEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(5, 3));
        expected.add(new BoardCoord(5, 4));
        Assert.assertEquals(2, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.UPRIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(5, 4));
        expected.add(new BoardCoord(5, 5));
        Assert.assertEquals(2, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.RIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(4, 5));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.DOWNRIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(3, 3));
        expected.add(new BoardCoord(3, 4));
        Assert.assertEquals(2, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.DOWNLEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(3, 2));
        expected.add(new BoardCoord(3, 3));
        Assert.assertEquals(2, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.LEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(4, 2));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));


        // Three balls

        // horizontally aligned
        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 4));
        balls.add(new BoardCoord(4, 3));
        balls.add(new BoardCoord(4, 2));


        targets = game.getTargetsOfMove(balls, Direction.UPLEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(5, 2));
        expected.add(new BoardCoord(5, 3));
        expected.add(new BoardCoord(5, 4));
        Assert.assertEquals(3, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.UPRIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(5, 3));
        expected.add(new BoardCoord(5, 4));
        expected.add(new BoardCoord(5, 5));
        Assert.assertEquals(3, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.RIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(4, 5));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.DOWNRIGHT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(3, 2));
        expected.add(new BoardCoord(3, 3));
        expected.add(new BoardCoord(3, 4));
        Assert.assertEquals(3, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.DOWNLEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(3, 1));
        expected.add(new BoardCoord(3, 2));
        expected.add(new BoardCoord(3, 3));
        Assert.assertEquals(3, targets.size());
        Assert.assertTrue(targets.containsAll(expected));

        targets = game.getTargetsOfMove(balls, Direction.LEFT);
        expected = new ArrayList<>();
        expected.add(new BoardCoord(4, 1));
        Assert.assertEquals(1, targets.size());
        Assert.assertTrue(targets.containsAll(expected));
    }

    @Test
    public void testCanMove() {

        // les boules sont dans l'état initial
        Game game = new Game();

        List<BoardCoord> balls = null;
        Direction direction = null;

        // One ball

        // mouvements autorisés
        // il n'y a pas de boule autour
        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 3));
        Assert.assertTrue(game.canMove(balls, Direction.UPLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.UPRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.RIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.LEFT));

        // mouvements non autorisés

        // on se place à côté des boules noires (déjà présentes)
        balls = new ArrayList<>();
        balls.add(new BoardCoord(5, 4));
        Assert.assertFalse(game.canMove(balls, Direction.UPLEFT));
        Assert.assertFalse(game.canMove(balls, Direction.UPRIGHT));

        // on se place à côté des boules blanches (déjà présentes)
        balls = new ArrayList<>();
        balls.add(new BoardCoord(3, 4));
        Assert.assertFalse(game.canMove(balls, Direction.DOWNRIGHT));
        Assert.assertFalse(game.canMove(balls, Direction.DOWNLEFT));

        // on se place sur le bord du plateau
        balls = new ArrayList<>();
        balls.add(new BoardCoord(2, 6));
        Assert.assertFalse(game.canMove(balls, Direction.RIGHT));
        Assert.assertFalse(game.canMove(balls, Direction.DOWNRIGHT));

        // Two balls

        // no ball around

        // horizontally aligned
        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 4));
        balls.add(new BoardCoord(4, 3));

        Assert.assertTrue(game.canMove(balls, Direction.UPLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.UPRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.RIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.LEFT));

        // UPLEFT-DOWNRIGHT axis
        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 2));
        balls.add(new BoardCoord(5, 2));

        Assert.assertTrue(game.canMove(balls, Direction.UPLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.UPRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.RIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.LEFT));

        // DOWNLEFT-UPRIGHT axis
        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 2));
        balls.add(new BoardCoord(3, 1));

        Assert.assertTrue(game.canMove(balls, Direction.UPLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.UPRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.RIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.LEFT));

        // ball is blocking


        // Sumito


        // Three balls

        // pas de boule autour

        balls = new ArrayList<>();
        balls.add(new BoardCoord(4, 4));
        balls.add(new BoardCoord(4, 3));
        balls.add(new BoardCoord(4, 2));


        Assert.assertTrue(game.canMove(balls, Direction.UPLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.UPRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.RIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNRIGHT));
        Assert.assertTrue(game.canMove(balls, Direction.DOWNLEFT));
        Assert.assertTrue(game.canMove(balls, Direction.LEFT));


    }

    @Test
    public void testDoMove() {
        Game game = new Game();


        // One ball

        List<BoardCoord> balls = new ArrayList<>();

        game.doMove(balls, Direction.UPLEFT);

    }
}
