package fr.cpe.abalown.models;

import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.utils.InitCoordProvider;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GameMoveCheckerTest {

    private static Game game;
    private static GameMoveChecker gameMoveChecker;

    @BeforeClass
    public static void init() {
        Player blackPlayer = new Player(Color.BLACK);
        Player whitePlayer = new Player(Color.WHITE);

        blackPlayer.setBoardBalls(new BoardBalls(InitCoordProvider.getBlackInitCoord()));
        whitePlayer.setBoardBalls(new BoardBalls(InitCoordProvider.getWhiteInitCoord()));

        game = new Game();
        gameMoveChecker = new GameMoveChecker(
                blackPlayer.getBoardBalls().getBalls(),
                whitePlayer.getBoardBalls().getBalls()
        );
    }

    @Test
    public void testIsCoordOnBoard() {

        System.out.println(game);

        List<BoardCoord> goodCoords = new ArrayList<>();
        goodCoords.add(new BoardCoord(4,8));
        goodCoords.add(new BoardCoord(8,8));
        goodCoords.add(new BoardCoord(3,7));
        goodCoords.add(new BoardCoord(0,4));
        goodCoords.add(new BoardCoord(8,4));
        goodCoords.add(new BoardCoord(0,4));
        goodCoords.add(new BoardCoord(0,0));

        List<BoardCoord> wrongCoords = new ArrayList<>();
        wrongCoords.add(new BoardCoord(3,8));
        wrongCoords.add(new BoardCoord(0,8));
        wrongCoords.add(new BoardCoord(1,6));
        wrongCoords.add(new BoardCoord(0,9));
        wrongCoords.add(new BoardCoord(9,0));
        wrongCoords.add(new BoardCoord(5,0));
        wrongCoords.add(new BoardCoord(7,2));

        for (BoardCoord coord : goodCoords) {
            Assert.assertTrue(gameMoveChecker.isCoordOnBoard(coord));
        }

        for (BoardCoord coord : wrongCoords) {
            Assert.assertFalse(gameMoveChecker.isCoordOnBoard(coord));
        }
    }

    @Test
    public void testIsBallHere() {

        System.out.println(game);

        List<BoardCoord> goodCoords = new ArrayList<>();
        goodCoords.add(new BoardCoord(8,8));
        goodCoords.add(new BoardCoord(6,6));
        goodCoords.add(new BoardCoord(1,0));

        List<BoardCoord> wrongCoords = new ArrayList<>();
        wrongCoords.add(new BoardCoord(4,8));
        wrongCoords.add(new BoardCoord(2,1));
        wrongCoords.add(new BoardCoord(6,7));

        for (BoardCoord coord : goodCoords) {
            Assert.assertTrue(gameMoveChecker.isBallHere(coord));
        }

        for (BoardCoord coord : wrongCoords) {
            Assert.assertFalse(gameMoveChecker.isBallHere(coord));
        }
    }

    @Test
    public void testIsPlayerBallHere() {

        System.out.println(game);

        Player blackPlayer = new Player(Color.BLACK);
        Player whitePlayer = new Player(Color.WHITE);

        List<BoardCoord> goodBlackCoords = new ArrayList<>();
        goodBlackCoords.add(new BoardCoord(8,4));
        goodBlackCoords.add(new BoardCoord(6,5));
        goodBlackCoords.add(new BoardCoord(7,3));

        List<BoardCoord> goodWhiteCoords = new ArrayList<>();
        goodWhiteCoords.add(new BoardCoord(0,0));
        goodWhiteCoords.add(new BoardCoord(2,3));
        goodWhiteCoords.add(new BoardCoord(1,4));

        List<BoardCoord> wrongBlackCoords = new ArrayList<>();
        wrongBlackCoords.add(new BoardCoord(0,0));
        wrongBlackCoords.add(new BoardCoord(4,0));
        wrongBlackCoords.add(new BoardCoord(6,8));

        List<BoardCoord> wrongWhiteCoords = new ArrayList<>();
        wrongWhiteCoords.add(new BoardCoord(8,5));
        wrongWhiteCoords.add(new BoardCoord(2,0));
        wrongWhiteCoords.add(new BoardCoord(5,7));

        for (BoardCoord coord : goodBlackCoords) {
            Assert.assertTrue(gameMoveChecker.isPlayerBallHere(blackPlayer, coord));
        }
        for (BoardCoord coord : goodWhiteCoords) {
            Assert.assertTrue(gameMoveChecker.isPlayerBallHere(whitePlayer, coord));
        }
        for (BoardCoord coord : wrongBlackCoords) {
            Assert.assertFalse(gameMoveChecker.isPlayerBallHere(blackPlayer, coord));
        }
        for (BoardCoord coord : wrongWhiteCoords) {
            Assert.assertFalse(gameMoveChecker.isPlayerBallHere(whitePlayer, coord));
        }
    }

    @Test
    public void testIsNeigbor() {

        BoardCoord ballOne;
        BoardCoord ballTwo;
        ballOne = new BoardCoord(4,3);


        ballTwo = new BoardCoord(4, 2);
        Assert.assertTrue(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(5, 3);
        Assert.assertTrue(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(5, 4);
        Assert.assertTrue(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(4, 4);
        Assert.assertTrue(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(3, 3);
        Assert.assertTrue(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(3, 2);
        Assert.assertTrue(gameMoveChecker.isNeighbor(ballOne, ballTwo));



        ballTwo = new BoardCoord(4, 3);
        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(4, 5);
        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(4, 1);
        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(5, 5);
        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(6, 4);
        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(5, 2);
        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(3, 1);
        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        ballTwo = new BoardCoord(3, 4);
        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, ballTwo));

        Assert.assertFalse(gameMoveChecker.isNeighbor(ballOne, null));
        Assert.assertFalse(gameMoveChecker.isNeighbor(null, ballTwo));

    }

}
