package fr.cpe.abalown.models;

import fr.cpe.abalown.enums.Color;
import fr.cpe.abalown.enums.Direction;
import fr.cpe.abalown.utils.BoardCoordFinder;
import fr.cpe.abalown.utils.InitCoordProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BoardCoordFinderTest {

    private static Game game;

    @BeforeClass
    public static void init() {
        game = new Game();
    }

    @Test
    public void testFindCoords() {

        System.out.println(game);

        List<BoardCoord> coords = new ArrayList<>();
        coords.add(new BoardCoord(8,5));
        coords.add(new BoardCoord(7,5));
        coords.add(new BoardCoord(6,5));

        System.out.println(BoardCoordFinder.findCoords(coords, Direction.DOWNLEFT));
    }
}
