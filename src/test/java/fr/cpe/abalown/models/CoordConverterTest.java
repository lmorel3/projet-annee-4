package fr.cpe.abalown.models;

import fr.cpe.abalown.utils.CoordConverter;
import org.junit.Assert;
import org.junit.Test;

public class CoordConverterTest {

    @Test
    public void getVisualCoordFromBoardCoordTest() {

//        int x = 0;
//        int y = 0;
//
//        VisualCoord expected = new VisualCoord(509, 600);
//        VisualCoord result = CoordConverter.getVisualCoordFromBoardCoord(x, y);
//
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(567, 600);
//        result = CoordConverter.getVisualCoordFromBoardCoord(1, 0);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(625, 600);
//        result = CoordConverter.getVisualCoordFromBoardCoord(2, 0);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(683, 600);
//        result = CoordConverter.getVisualCoordFromBoardCoord(3, 0);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(480, 542);
//        result = CoordConverter.getVisualCoordFromBoardCoord(0, 1);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(451, 484);
//        result = CoordConverter.getVisualCoordFromBoardCoord(0, 2);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(422, 426);
//        result = CoordConverter.getVisualCoordFromBoardCoord(0, 3);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(393, 368);
//        result = CoordConverter.getVisualCoordFromBoardCoord(0, 4);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(422, 310);
//        result = CoordConverter.getVisualCoordFromBoardCoord(1, 5);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(451, 252);
//        result = CoordConverter.getVisualCoordFromBoardCoord(2, 6);
//        Assert.assertEquals(expected, result);
//
//
//        expected = new VisualCoord(538, 426);
//        result = CoordConverter.getVisualCoordFromBoardCoord(2, 3);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(857, 368);
//        result = CoordConverter.getVisualCoordFromBoardCoord(8, 4);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(509, 136);
//        result = CoordConverter.getVisualCoordFromBoardCoord(4, 8);
//        Assert.assertEquals(expected, result);
//
//
//        // moitié haute
//        expected = new VisualCoord(538, 426);
//        result = CoordConverter.getVisualCoordFromBoardCoord(2, 3);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(828, 426);
//        result = CoordConverter.getVisualCoordFromBoardCoord(7, 3);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(857, 368);
//        result = CoordConverter.getVisualCoordFromBoardCoord(8, 4);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(509, 136);
//        result = CoordConverter.getVisualCoordFromBoardCoord(4, 8);
//        Assert.assertEquals(expected, result);
//
//        expected = new VisualCoord(741, 136);
//        result = CoordConverter.getVisualCoordFromBoardCoord(8, 8);
//        Assert.assertEquals(expected, result);

    }

    @Test
    public void getBoardCoordFromVisualCoordTest() {

//        BoardCoord expected;
//        BoardCoord result;
//
//        expected = new BoardCoord(0, 0);
//        result = CoordConverter.getBoardCoordFromVisualCoord(509+1, 600+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(1, 0);
//        result = CoordConverter.getBoardCoordFromVisualCoord(567+2, 600+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(2, 0);
//        result = CoordConverter.getBoardCoordFromVisualCoord(625+1, 600+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(3, 0);
//        result = CoordConverter.getBoardCoordFromVisualCoord(683+1, 600+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(0, 1);
//        result = CoordConverter.getBoardCoordFromVisualCoord(480+1, 542+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(0, 2);
//        result = CoordConverter.getBoardCoordFromVisualCoord(451+1, 484+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(0, 3);
//        result = CoordConverter.getBoardCoordFromVisualCoord(422+1, 426+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(0, 4);
//        result = CoordConverter.getBoardCoordFromVisualCoord(393+1, 368+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(1, 5);
//        result = CoordConverter.getBoardCoordFromVisualCoord(422+1, 310+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(2, 6);
//        result = CoordConverter.getBoardCoordFromVisualCoord(451+1, 252+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(2, 3);
//        result = CoordConverter.getBoardCoordFromVisualCoord(538+1, 426+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(7, 3);
//        result = CoordConverter.getBoardCoordFromVisualCoord(828+1, 426+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(8, 4);
//        result = CoordConverter.getBoardCoordFromVisualCoord(857+1, 368+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(4, 8);
//        result = CoordConverter.getBoardCoordFromVisualCoord(509+1, 136+1);
//        Assert.assertEquals(expected, result);
//
//        expected = new BoardCoord(8, 8);
//        result = CoordConverter.getBoardCoordFromVisualCoord(741+1, 136+1);
//        Assert.assertEquals(expected, result);
    }
}